from rest_framework import generics, status, viewsets
from OBVapp.api.serializers import (OBVindexSerializers)

from Main_app.models import StockDayData
from rest_framework.generics import get_object_or_404

from Main_app.api.serializers import StockDayDataSerializers, StockDayDataListSerializers
#from drf_multiple_model.views import ObjectMultipleModelAPIView
#from drf_multiple_model.pagination import MultipleModelLimitOffsetPagination

from rest_framework.renderers import JSONRenderer

from rest_framework import renderers

class StockDayDataViewSet(viewsets.ModelViewSet):
    #renderer_classes = [renderers.JSONRenderer]

    serializer_class = StockDayDataSerializers
    queryset = StockDayData.objects.filter().order_by('-stock_date').all()

    # def get_queryset(self):
    #     print(" ---------------check---------------")
    #     kwarg_pk = self.kwargs.get("pk")
    #     print(" kwarg_pk  ",kwarg_pk)
    #     return StockDayData.objects.filter(company_stock_data__id=kwarg_pk).order_by('-stock_date').all()


class StockDayDataListView(generics.ListAPIView):
    serializer_class = StockDayDataListSerializers
    

    def get_queryset(self,  *args, **kwargs):
        print(" ---------------check  view---------------")
        
        kwarg_pk = self.kwargs.get("pk_company")
         
        print(" kwarg_pk  ",kwarg_pk)
        return StockDayData.objects.filter(company_stock_data__id=kwarg_pk).order_by('stock_date').all()